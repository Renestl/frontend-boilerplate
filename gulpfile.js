var gulp = require('gulp');
var bs = require('browser-sync').create();

const sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var fs = require('fs');
var path = require('path');
var merge = require('merge-stream');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

// TODO: add mocha and chai testing

var scssInput = './app/assets/';

var sassOptions = {
	errLogToConsole: true,
	outputStyle: 'expanded'
};

function getFolders(dir) {
	return fs.readdirSync(dir)
		.filter(function(file) {
			return fs.statSync(path.join(dir, file)).isDirectory();
		});
}

// Compile Sass
gulp.task('sass', function() {

	var processors = [
		autoprefixer({
			browsers: ['last 4 versions']
		})
	];

	var folders = getFolders(scssInput);
	
	var tasks = folders.map(function() {
		var src = path.join(scssInput, 'scss');
		var dst = path.join(scssInput, 'css');

		return gulp.src(path.join(src, '*.scss'))
			.pipe(sourcemaps.init())
			.pipe(sass(sassOptions).on('error', sass.logError))
			.pipe(postcss(processors))
			.pipe(sourcemaps.write('./maps'))
			.pipe(gulp.dest(dst))
			.pipe(bs.stream()); 
	});

	return merge(tasks);
});

// Watch and Server
gulp.task('serve', ['sass'], function() {
	bs.init({
		server: './app'
	});

	gulp.watch(['app/assets/scss/**/*.scss'], ['sass']);
	gulp.watch(['**/**.html']).on('change', bs.reload);
});

// Default
gulp.task('default', ['serve']);