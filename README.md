# HTML5 Boilerplate


## Installation

Follow the steps below to have your (frontend) boilerplate up and ready to start coding.

* Download (fork, direct download, clone) the project to your local machine
* `npm install` on the source directory to install all the devDependencies (for running gulp)

## Includes
* Gulp
* Autoprefixer
* Babel
* Browser-sync
* Sass
* Mocha

# TO DO:
* eslint
* normalize
* prettier
* gulp
* webpack
* mocha
* autoprefixer
* browser-sync

### Sites
https://github.com/h5bp/html5-boilerplate
https://github.com/DeviaVir/boilerplate
https://github.com/imarc/boilerplate
https://github.com/corysimmons/boy

## License
MIT